# Custom Driver for the Huion Kamvas GT-191 running on linux

## Information before use

>  This driver can work really good but it uses a lot of cpu-power compared to other solutions. 
   I recommend you to use the [DIGImend kernel module](https://digimend.github.io/) which should also work for this tablet 
   (at least on ArchLinux if you use this AUR package: https://aur.archlinux.org/packages/digimend-kernel-drivers-dkms-git).

## Inspiration and motivation

I bought a Huion Kamvas GT-191 after checking out if there is any driver or support for such devices which could run on arch-linux. So I found this repository here: https://github.com/benthor/HuionKamvasGT191LinuxDriver

After I got the device and checking out if the "quick & dirty"-driver in python worked which was actually pretty fine, I looked up the methods which were used to receive events from the usb-connection and using uinput to combine pretty much in one compact program written in C.

## Features

This driver shall be able to transfer all events from the device to usable events in the system and using just the part of the screen which is linked to the device itself to work in accurate projection-mode like other graphic-tablets etc..

A new feature is that if you have xinput installed, you can run the driver with full possibility to calibrate it through xinput.

Alternatively or additionally you can use the calibrate-script which calls the driver with an implementation of a calibration-test with dynamic learning using a neuronal network. Every full execution of the script grants 21 new data-points which will change the weights of the links.

## Dependencies

Currently it requires libusb to catch all necessary data for the events, libX11 and Xrandr to get enough information about the screens and monitors for a fine projection.

- libusb
- libX11
- libXrandr

It will use one of my own libraries for neuronal networks (using for dynamic calibration). But you can get this just by calling the setup-script before compiling.

- libnnc

## Requirements on arch-linux

You will need those packages to compile and run the driver:
```
sudo pacman -S libusb libx11 libxrandr xf86-input-evdev
```

## Optional packages

If you install this you can use additional features of this driver:
```
sudo pacman -S xorg-xinput
```

## Note

It's only tested on arch-linux so far. Please let me know if anything works for you with other distributions or more important if it's not. ^_^
