/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CWindow.h"

#include <X11/Xutil.h>

struct MwmHints {
	unsigned long flags;
	unsigned long functions;
	unsigned long decorations;

	long input_mode;

	unsigned long status;
};

void openWindow(CWindow* window, Monitor* monitor, int mode) {
	window->display = XOpenDisplay(NULL);

	window->index = 0;
	window->mode = mode;

	Screen* screen = XScreenOfDisplay(window->display, monitor->screen_number);

	XVisualInfo vinfo;
	XMatchVisualInfo(window->display, XScreenNumberOfScreen(screen), 32, TrueColor, &vinfo);

	Window root = XRootWindow(window->display, XScreenNumberOfScreen(screen));

	XSetWindowAttributes attr;
	attr.colormap = XCreateColormap(window->display, root, vinfo.visual, AllocNone);
	attr.border_pixel = 0;
	attr.background_pixel = BlackPixel(window->display, XScreenNumberOfScreen(screen));

	window->window = XCreateWindow(
			window->display, root,
			monitor->x,
			monitor->y,
			(uint) monitor->width,
			(uint) monitor->height,
			0, vinfo.depth,
			InputOutput,
			vinfo.visual,
			CWColormap | CWBorderPixel | CWBackPixel,
			&attr
	);

	Atom mwmHintsProperty = XInternAtom(window->display, "_MOTIF_WM_HINTS", 0);

	struct MwmHints hints;
	hints.flags = (1L << 1); // MWM_HINTS_DECORATIONS
	hints.decorations = 0;

	XChangeProperty(
			window->display,
			window->window,
			mwmHintsProperty, mwmHintsProperty,
			32, PropModeReplace,
			(unsigned char*) &hints, 5
	);

	XSelectInput(window->display, window->window, StructureNotifyMask | ExposureMask);

	XGCValues gc_val;
	gc_val.foreground = WhitePixel(window->display, XScreenNumberOfScreen(screen));
	gc_val.background = BlackPixel(window->display, XScreenNumberOfScreen(screen));
	gc_val.line_width = 5;

	window->gc = XCreateGC(
			window->display,
			window->window,
			GCForeground | GCBackground | GCLineWidth,
			&gc_val
	);

	XColor red;

	if (XAllocNamedColor(window->display, attr.colormap, "red", &red, &red)) {
		XSetForeground(window->display, window->gc, red.pixel);
	}

	Atom wm_delete_window = XInternAtom(window->display, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(window->display, window->window, &wm_delete_window, 1);

	XMapWindow(window->display, window->window);

	XMoveResizeWindow(
			window->display,
			window->window,
			monitor->x,
			monitor->y,
			(uint) monitor->width,
			(uint) monitor->height
	);
}

void drawPoint(Display* display, Window window, GC gc, int x, int y) {
	XDrawArc(
			display,
			window,
			gc,
			x - 30,
			y - 30,
			60, 60,
			0, 360 * 64
	);

	XDrawArc(
			display,
			window,
			gc,
			x - 5,
			y - 5,
			10, 10,
			0, 360 * 64
	);
}

void pointByIndex(int mode, int width, int height, size_t index, int* x, int* y) {
	switch (mode) {
		case CW_MODE_CALIBRATION: {
			size_t size = 1;
			size_t line = 1;

			while (index >= size) {
				index -= size;
				size <<= 2;
				line <<= 1;
			}

			*x = (int) (width * (0.5f + (index % line)) / line);
			*y = (int) (height * (0.5f + (index / line)) / line);
			break;
		} case CW_MODE_CORNERS:
			*x = (int) (width * (index % 2));
			*y = (int) (height * (index / 2));
			break;
		case CW_MODE_RAND10:
			*x = (int) ((size_t) width * rand() / RAND_MAX);
			*y = (int) ((size_t) height * rand() / RAND_MAX);
			break;
		default:
			break;
	}
}

void getWindowPoint(CWindow* window, int* x, int* y) {
	if (window->display != NULL) {
		XWindowAttributes attr;

		XGetWindowAttributes(window->display, window->window, &attr);

		pointByIndex(window->mode, attr.width, attr.height, window->index, x, y);
	}
}

void drawScene(Display* display, Window window, GC gc, int width, int height, size_t index, int mode) {
	XClearArea(
			display,
			window,
			0, 0,
			(uint) width,
			(uint) height,
			False
	);

	int x, y;

	pointByIndex(mode, width, height, index, &x, &y);

	drawPoint(display, window, gc, x, y);
}

void nextWindowPoint(CWindow* window, Monitor* monitor) {
	window->index++;

	XClearArea(
			window->display,
			window->window,
			0, 0,
			(uint) monitor->width,
			(uint) monitor->height,
			True
	);
}

bool updateWindow(CWindow* window) {
	if (window->display != NULL) {
		XEvent event;

		while (XPending(window->display) > 0) {
			XNextEvent(window->display, &event);

			switch (event.type) {
				case ClientMessage:
					if ((event.xclient.message_type == XInternAtom(window->display, "WM_PROTOCOLS", 1)) &&
						((Atom) event.xclient.data.l[0] == XInternAtom(window->display, "WM_DELETE_WINDOW", 1))) {
						return false;
					}

					break;
				case Expose:
					drawScene(
							event.xexpose.display,
							event.xexpose.window,
							window->gc,
							event.xexpose.width,
							event.xexpose.height,
							window->index,
							window->mode
					);

					break;
				default:
					break;
			}
		}
	}

	return true;
}

void closeWindow(CWindow* window) {
	if (window->display != NULL) {
		XDestroyWindow(window->display, window->window);

		XCloseDisplay(window->display);
		window->display = NULL;
	}
}
