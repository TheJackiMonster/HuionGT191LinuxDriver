#pragma once

/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Device.h"
#include "Monitor.h"
#include "Calibration.h"
#include "CWindow.h"

#define HANDLE_STATUS_EXIT 			0x01
#define HANDLE_STATUS_NONE 			0xFF
#define HANDLE_STATUS_OK 			0x00
#define HANDLE_STATUS_ERROR_LIBUSB 	0x02
#define HANDLE_STATUS_ERROR_UINPUT 	0x04

#define READ_LENGTH_TO_FIX 32

typedef struct handle_t {
	Device device;
	Monitor monitor;
	Calibration calibration;
	CWindow window;

	int uinput_fd;

	bool use_device;
	char use_monitor;
	bool use_xinput;
	bool use_cwindow;

	bool fix_hid_uclogic;
	bool use_uinput;

	int cw_mode;

	int min_x;
	int min_y;
	int max_x;
	int max_y;
} Handle;

bool openHandle(Handle* handle);

bool updateHandle(Handle* handle);

bool closeHandle(Handle* handle);
