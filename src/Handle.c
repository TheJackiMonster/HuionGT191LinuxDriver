/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Handle.h"

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

bool fix_hid_uclogic() {
	char output[ READ_LENGTH_TO_FIX ];
	
	FILE* fd = popen("cat /proc/modules | grep hid_uclogic", "r");
	
	if(fd == NULL) {
		return false;
	}
	
	size_t len = fread(output, sizeof(char), READ_LENGTH_TO_FIX - 1, fd);
	
	if (len > 0) {
		system("sudo modprobe -r hid_uclogic");
	}

	pclose(fd);
	return true;
}

bool openHandle(Handle* handle) {
	openCalibration(&(handle->calibration));

	handle->use_device = findDevice(&(handle->device));
	handle->use_monitor = findMonitor(&(handle->monitor));
	handle->use_xinput = (system("xinput") == 0);
	
	handle->fix_hid_uclogic = fix_hid_uclogic();
	handle->use_uinput = (system("sudo modprobe uinput") == 0);

	if ((handle->cw_mode != CW_MODE_NONE) && (handle->use_monitor != 0)) {
		openWindow(&(handle->window), &(handle->monitor), handle->cw_mode);

		handle->use_cwindow = true;
	} else {
		handle->use_cwindow = false;
	}

	if (((handle->use_device) && (handle->fix_hid_uclogic)) && (handle->use_uinput)) {
		handle->uinput_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

		if (handle->uinput_fd > 0) {
#ifdef UI_DEV_SETUP
			struct uinput_abs_setup abs_x, abs_y, abs_pressure;
			
			ioctl(handle->uinput_fd, UI_SET_PROPBIT, INPUT_PROP_DIRECT);
			ioctl(handle->uinput_fd, UI_SET_PROPBIT, INPUT_PROP_POINTER);

			ioctl(handle->uinput_fd, UI_SET_EVBIT,  EV_KEY);

			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_TOOL_PEN);

			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_TOUCH);
			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_STYLUS);
			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_STYLUS2);

			ioctl(handle->uinput_fd, UI_SET_EVBIT,  EV_ABS);

			memset(&abs_x, 0, sizeof(abs_x));

			abs_x.code = ABS_X;
			abs_x.absinfo.fuzz = 5;
			abs_x.absinfo.resolution = handle->device.resolution;

			if ((handle->use_monitor != 0) && (!handle->use_xinput)) {
				abs_x.absinfo.minimum = -handle->device.maxX * handle->monitor.x / handle->monitor.width;
				abs_x.absinfo.maximum = +handle->device.maxX * (handle->monitor.screen_width - handle->monitor.x) / handle->monitor.width;

				handle->min_x = handle->device.maxX * handle->monitor.x / handle->monitor.screen_width;
				handle->max_x = handle->device.maxX * (handle->monitor.x + handle->monitor.width) / handle->monitor.screen_width;
			} else {
				abs_x.absinfo.maximum = handle->device.maxX;

				handle->min_x = 0;
				handle->max_x = handle->device.maxX;
			}

			ioctl(handle->uinput_fd, UI_ABS_SETUP, &abs_x);

			memset(&abs_y, 0, sizeof(abs_y));

			abs_y.code = ABS_Y;
			abs_y.absinfo.fuzz = 5;
			abs_y.absinfo.resolution = handle->device.resolution;

			if ((handle->use_monitor != 0) && (!handle->use_xinput)) {
				abs_y.absinfo.minimum = -handle->device.maxY * handle->monitor.y / handle->monitor.height;
				abs_y.absinfo.maximum = +handle->device.maxY * (handle->monitor.screen_height - handle->monitor.y) / handle->monitor.height;

				handle->min_y = handle->device.maxY * handle->monitor.y / handle->monitor.screen_height;
				handle->max_y = handle->device.maxY * (handle->monitor.y + handle->monitor.height) / handle->monitor.screen_height;
			} else {
				abs_y.absinfo.maximum = handle->device.maxY;

				handle->min_y = 0;
				handle->max_y = handle->device.maxY;
			}

			ioctl(handle->uinput_fd, UI_ABS_SETUP, &abs_y);

			memset(&abs_pressure, 0, sizeof(abs_pressure));

			abs_pressure.code = ABS_PRESSURE;
			abs_pressure.absinfo.maximum = handle->device.maxPressure;

			ioctl(handle->uinput_fd, UI_ABS_SETUP, &abs_pressure);

			struct uinput_setup setup;

			memset(&setup, 0, sizeof(setup));

			setup.id.bustype = BUS_USB;
			setup.id.vendor = handle->device.vendorId;
			setup.id.product = handle->device.productId;
			setup.id.version = 0x3;

			strcpy(setup.name, handle->device.name);

			ioctl(handle->uinput_fd, UI_DEV_SETUP, &setup);

			if (ioctl(handle->uinput_fd, UI_DEV_CREATE) != 0) {
				close(handle->uinput_fd);

				handle->uinput_fd = 0;
			} else {
				sleep(1);
			}
#else
			struct uinput_user_dev uud;
			
			ioctl(handle->uinput_fd, UI_SET_PROPBIT, INPUT_PROP_DIRECT);
			ioctl(handle->uinput_fd, UI_SET_PROPBIT, INPUT_PROP_POINTER);

			ioctl(handle->uinput_fd, UI_SET_EVBIT,  EV_KEY);

			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_TOOL_PEN);

			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_TOUCH);
			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_STYLUS);
			ioctl(handle->uinput_fd, UI_SET_KEYBIT, BTN_STYLUS2);

			ioctl(handle->uinput_fd, UI_SET_EVBIT,  EV_ABS);

			ioctl(handle->uinput_fd, UI_SET_ABSBIT, ABS_X);
			ioctl(handle->uinput_fd, UI_SET_ABSBIT, ABS_Y);
			ioctl(handle->uinput_fd, UI_SET_ABSBIT, ABS_PRESSURE);

			memset(&uud, 0, sizeof(uud));

			uud.id.bustype = BUS_USB;
			uud.id.vendor = handle->device.vendorId;
			uud.id.product = handle->device.productId;
			uud.id.version = 0x3;

			strcpy(uud.name, handle->device.name);

			uud.absfuzz[0] = 5;

			if ((handle->use_monitor != 0) && (!handle->use_xinput)) {
				uud.absmin[0] = -handle->device.maxX * handle->monitor.x / handle->monitor.width;
				uud.absmax[0] = +handle->device.maxX * (handle->monitor.screen_width - handle->monitor.x) / handle->monitor.width;

				handle->min_x = handle->device.maxX * handle->monitor.x / handle->monitor.screen_width;
				handle->max_x = handle->device.maxX * (handle->monitor.x + handle->monitor.width) / handle->monitor.screen_width;
			} else {
				uud.absmax[0] = handle->device.maxX;

				handle->min_x = 0;
				handle->max_x = handle->device.maxX;
			}

			uud.absfuzz[1] = 5;

			if ((handle->use_monitor != 0) && (!handle->use_xinput)) {
				uud.absmin[1] = -handle->device.maxY * handle->monitor.y / handle->monitor.height;
				uud.absmax[1] = +handle->device.maxY * (handle->monitor.screen_height - handle->monitor.y) / handle->monitor.height;

				handle->min_y = handle->device.maxY * handle->monitor.y / handle->monitor.screen_height;
				handle->max_y = handle->device.maxY * (handle->monitor.y + handle->monitor.height) / handle->monitor.screen_height;
			} else {
				uud.absmax[1] = handle->device.maxY;

				handle->min_y = 0;
				handle->max_y = handle->device.maxY;
			}

			uud.absmax[2] = handle->device.maxPressure;

			write(handle->uinput_fd, &uud, sizeof(uud));

			if (ioctl(handle->uinput_fd, UI_DEV_CREATE) != 0) {
				close(handle->uinput_fd);

				handle->uinput_fd = 0;
			} else {
				sleep(1);
			}
#endif
		}

		if (handle->uinput_fd != 0) {
			if ((handle->use_monitor != 0) && (handle->use_xinput)) {
				char cmd [1024];

				sprintf(cmd, "xinput set-prop \"%s\" --type=float \"Coordinate Transformation Matrix\" %f 0 %f 0 %f %f 0 0 1\0",
						handle->device.name,
						(float) handle->monitor.width / handle->monitor.screen_width,
						(float) handle->monitor.x / handle->monitor.screen_width,
						(float) handle->monitor.height / handle->monitor.screen_height,
						(float) handle->monitor.y / handle->monitor.screen_height
				);

				system(cmd);
			}

			return true;
		}
	} else {
		handle->uinput_fd = 0;
	}

	return false;
}

void calibrate(Handle* handle, int x, int y, int ex, int ey) {
	addPairToCalibration(
			&(handle->calibration),
			((nnc_val) x + 0.5) / handle->device.maxX,
			((nnc_val) y + 0.5) / handle->device.maxY,
			((nnc_val) ex + 0.5) / handle->monitor.width,
			((nnc_val) ey + 0.5) / handle->monitor.height
	);
}

bool emitEvent(int fd, __u16 type, __u16 code, int value, uint delay) {
	static struct input_event event;

	event.type = type;
	event.code = code;
	event.value = value;

	if (delay > 0) {
		gettimeofday(&(event.time), NULL);

		event.time.tv_sec += delay;
	} else {
		event.time.tv_sec = 0;
		event.time.tv_usec = 0;
	}

	return (write(fd, &event, sizeof(event)) == sizeof(event));
}

bool updateHandle(Handle* handle) {
	if (!handle->use_device) {
		return false;
	}

	unsigned char status = HANDLE_STATUS_NONE;

	if (openDevice(&(handle->device))) {
		status = HANDLE_STATUS_OK;
	}

	if (status == HANDLE_STATUS_OK) {
		Event event;

		memset(&event, 0, sizeof(event));

		int i, cx, cy;
		int r_event;

		const int fd = handle->uinput_fd;

		bool pen_active = false;

		const int btns[3] = { BTN_TOUCH, BTN_STYLUS, BTN_STYLUS2 };

		bool keys[3] = { false, false, false };

		nnc_val nx, ny;

		while (status == HANDLE_STATUS_OK) {
			if (handle->use_cwindow) {
				if (!updateWindow(&(handle->window))) {
					status |= HANDLE_STATUS_EXIT;
					continue;
				}
			}

			r_event = readDevice(&(handle->device), &event);

			if (r_event == 1) {
				if (handle->use_cwindow) {
					bool trigger = false;

					if (event.action & 1) {
						trigger = !keys[0];
						keys[0] = true;
					} else {
						trigger = keys[0];
						keys[0] = false;
					}

					if ((trigger) && (keys[0])) {
						getWindowPoint(&(handle->window), &cx, &cy);

						calibrate(handle, event.x, event.y, cx, cy);

						nextWindowPoint(&(handle->window), &(handle->monitor));

						if ((handle->cw_mode != CW_MODE_NONE) &&
							(handle->window.index >= CWModeSize[handle->cw_mode])) {
							status |= HANDLE_STATUS_EXIT;
							continue;
						}
					}
				} else {
					if (!pen_active) {
						if (!emitEvent(fd, EV_KEY, BTN_TOOL_PEN, 1, 0)) {
							status |= HANDLE_STATUS_ERROR_UINPUT;
						}

						pen_active = true;
					}

					readCalibration(
							&(handle->calibration),
							((nnc_val) event.x + 0.5) / handle->device.maxX,
							((nnc_val) event.y + 0.5) / handle->device.maxY,
							&nx,
							&ny
					);

					cx = (int) (handle->device.maxX * nx - 0.5);
					cy = (int) (handle->device.maxY * ny - 0.5);

					if (cx < handle->min_x) {
						cx = handle->min_x;
					} else
					if (cx > handle->max_x) {
						cx = handle->max_x;
					}

					if (cy < handle->min_y) {
						cy = handle->min_y;
					} else
					if (cy > handle->max_y) {
						cy = handle->max_y;
					}

					if (!(
							emitEvent(fd, EV_ABS, ABS_X, cx, 0) &
							emitEvent(fd, EV_ABS, ABS_Y, cy, 0) &

							emitEvent(fd, EV_ABS, ABS_PRESSURE, event.pressure, 0) &
							
							emitEvent(fd, EV_SYN, SYN_REPORT, 0, 0)
					)) {
						status |= HANDLE_STATUS_ERROR_UINPUT;
					}

					for (i = 0; i < 3; i++) {
						bool trigger = false;

						if (event.action & (1 << i)) {
							trigger = !keys[i];
							keys[i] = true;
						} else {
							trigger = keys[i];
							keys[i] = false;
						}

						if (trigger) {
							if (!(
									emitEvent(fd, EV_KEY, (__u16) btns[i], (keys[i]? 1 : 0), 0) &
									
									emitEvent(fd, EV_SYN, SYN_REPORT, 0, 0)
							)) {
								status |= HANDLE_STATUS_ERROR_UINPUT;
							}
						}
					}
				}
			} else
			if (r_event != 0) {
				if ((!closeDevice(&(handle->device))) || (!openDevice(&(handle->device)))) {
					status |= HANDLE_STATUS_ERROR_LIBUSB;
				}
			}
		}

		if (pen_active) {
			emitEvent(fd, EV_KEY, BTN_TOOL_PEN, 0, 0);
			emitEvent(fd, EV_SYN, SYN_REPORT, 0, 0);
		}

		if (handle->use_cwindow) {
			writePairsToCalibration(&(handle->calibration));
		}

		return closeDevice(&(handle->device));
	} else {
		return false;
	}
}

bool closeHandle(Handle* handle) {
	if (handle->use_cwindow) {
		closeWindow(&(handle->window));

		handle->use_cwindow = false;
	}

	closeCalibration(&(handle->calibration));

	if (handle->uinput_fd == 0) {
		return false;
	}

	sleep(1);

	ioctl(handle->uinput_fd, UI_DEV_DESTROY);

	close(handle->uinput_fd);

	handle->uinput_fd = 0;
	return true;
}
