#pragma once

/*
This is a free driver for the Huion Kamvas GT-191 running on linux.
Copyright (C) 2018  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Monitor.h"

#include <X11/Xlib.h>

#define CW_MODE_NONE 0x0
#define CW_MODE_CALIBRATION 0x1
#define CW_MODE_CORNERS 0x2
#define CW_MODE_RAND10 0x3

static const int CWModeSize [4] = {
		0,
		21,
		4,
		10
};

typedef struct cwindow_t {
	Display* display;
	Window window;

	GC gc;

	size_t index;
	int mode;
} CWindow;

void openWindow(CWindow* window, Monitor* monitor, int mode);

void getWindowPoint(CWindow* window, int* x, int* y);

void nextWindowPoint(CWindow* window, Monitor* monitor);

bool updateWindow(CWindow* window);

void closeWindow(CWindow* window);
